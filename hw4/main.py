import sys
from os.path import splitext
import math
from tqdm import tqdm
from PIL import Image

import cv2
import numpy as np


def get_gauss2D(sigma):
    gauss_range = np.array(list(range(-3 * int(sigma), 3 * int(sigma) + 1)), float)
    dom_x, dom_y = np.meshgrid(gauss_range, gauss_range)
    kernel = np.exp(-(dom_x ** 2 + dom_y ** 2) / (2 * sigma ** 2))
    return kernel


def get_gauss(sigma, x):
    return np.exp(-x ** 2 / (2 * sigma ** 2)) / (2 * math.pi * np.sqrt(sigma))


def apply_bilateral_filter(source, sigma_space, sigma_lum):
    # Get gaussian space kernel
    gauss_space = get_gauss2D(sigma_space)
    # Normalize
    gauss_space = gauss_space / np.sum(gauss_space)

    # Half of gauss_space matrix
    necessary_padding = int(sigma_space * 3)

    # Add padding to safely use larger matrix
    padded_source = np.pad(source, (necessary_padding, necessary_padding), mode="edge")
    result = np.zeros([len(padded_source), len(padded_source[0])])

    source_rows, source_columns = source.shape

    for x in tqdm(range(necessary_padding, source_rows + necessary_padding)):
        for y in range(necessary_padding, source_columns + necessary_padding):
            # Get area to be affected by filter
            filter_center = padded_source[x, y]
            filter_area = padded_source[x - necessary_padding: x + necessary_padding + 1,
                                        y - necessary_padding: y + necessary_padding + 1]

            # Get luminance gaussian kernel for current area
            gauss_lum = get_gauss(sigma_lum, np.subtract(filter_center, filter_area))
            # Create composed filter from both Gaussians
            combined_filter = gauss_space * gauss_lum

            # 1/sum not to overbrighten image
            result[x, y] = (1 / np.sum(combined_filter)) * np.sum(filter_area * combined_filter)

    return result[necessary_padding:source_rows + necessary_padding,
                  necessary_padding:source_columns + necessary_padding]


def main(file, sigma_space, sigma_lum):
    # load image in grayscale, setup gauss kernel
    image = np.asarray(Image.open(file).convert("L"), dtype="int32")

    # apply convolution
    image = apply_bilateral_filter(image, float(sigma_space), float(sigma_lum))

    # save
    name = splitext(file)[0] + "_convoluted_" + str(sigma_space) + "_" + str(sigma_lum) + splitext(file)[1]
    print(name)
    cv2.imwrite(name, image)


if __name__ == "__main__":
    args = sys.argv[1:]
    for i in range(0, len(args), 3):
        main(args[i], int(args[i + 1]), int(args[i + 2]))
