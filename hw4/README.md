# Úkol 4 - Nelineární filtrace

## Spuštění
```bash
python main.py <název souboru> <hodnota sigma pro prostorové gaussovské jádro> <hodnota sigma pro jasové gaussovské jádro> ...
```

## Předpoklady
Pro spuštění programu je třeba mít nainstalované pythnovské knihovny numpy, opencv.

```bash
pip install numpy opencv-python
```

## Běh programu

Nejdříve se vytvoří `Gaussovská matice` dle zadaných parametrů `sigma`. Vytvoříme 2D matici pro prostorovské gaussovské jádro a 1D matici pro jádro jasové. Jasové jádro se tvoří vždy závisle na filtrované oblasti.
Bilaterální filtr, který tyto 2 matice tvoří následně aplikujeme na zadaný obraz.

## Příklady
![original](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw4/building.png)*Originální obrázek*

| | Prostorová sigma=1 |  Prostorová sigma=5 |   Prostorová sigma=10 |
:-------------------------:|:-------------------------:|:-------------------------:|:-------------------------:
Jasová sigma=20  |![](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw4/building_convoluted_1_20.png)  |  ![](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw4/building_convoluted_5_20.png) |  ![](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw4/building_convoluted_10_20.png)
Jasová sigma=50  |![](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw4/building_convoluted_1_50.png)  |  ![](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw4/building_convoluted_5_50.png) |  ![](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw4/building_convoluted_10_50.png)
Jasová sigma=100 |![](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw4/building_convoluted_1_100.png)  |  ![](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw4/building_convoluted_5_100.png) |  ![](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw4/building_convoluted_10_100.png)