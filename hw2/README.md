# Úkol 2 - Fourierova transformace

## Spuštění
```bash
python main.cpp <jména obrázků pro které chceme získat amplitudová spektra>
```

## Předpoklady
Pro spuštění programu je třeba mít nainstalované pythnovské knihovny Pillow, numpy, opencv a matplotlib.

```bash
pip install Pillow numpy opencv-python matplotlib
```

## Běh programu

Po spuštění převede skript zadaný obrázek na vstupu do centrovaného amplitudového spektra. Do stejné složky, kde je obrázek, uloží tento obrázek v amplitudovém spektru a zároveň také kompletní trojici obrázků (originál, necentrované spektru, centrované spektrum) vedle sebe pro porovnání.

## Příklad

![](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw2/examples/pic1_amplitude_plt.jpg)
![](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw2/examples/pic3_amplitude_plt.jpg)
![](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw2/examples/pic4_amplitude_plt.jpg)
![](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw2/examples/pic5_amplitude_plt.jpg)