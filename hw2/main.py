from os.path import splitext

import cv2
import sys
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image


def get_amplitude(nparray):
    return np.log(1+np.sqrt([x.real ** 2 + x.imag ** 2 for x in nparray]))


def get_plt(imagename):
    plt.figure(figsize=(10 * 3, 8 * 3), constrained_layout=False)

    image = cv2.imread(imagename, 0)
    fourier_transform = np.fft.fft2(image)
    centered_spectrum = np.fft.ifftshift(fourier_transform)

    plt.subplot(131), plt.imshow(image, "gray"), plt.title("Original Image")
    plt.subplot(132), plt.imshow(get_amplitude(fourier_transform), "gray"), plt.title("Amplitude Spectrum")
    plt.subplot(133), plt.imshow(get_amplitude(centered_spectrum), "gray"), plt.title("Centered Amplitude Spectrum")
    plt.savefig(splitext(imagename)[0] + "_amplitude_plt" + splitext(imagename)[1])


def get_centered_ampl_spectrum(imagename):
    image = cv2.imread(imagename, 0)
    fourier_transform = np.fft.fft2(image)
    centered_spectrum = np.fft.ifftshift(fourier_transform)
    Image.fromarray(get_amplitude(centered_spectrum) * 20).convert('RGB').save(
        splitext(imagename)[0] + "_amplitude" + splitext(imagename)[1])


# for i in range(1, 6):
#     get_centered_ampl_spectrum("pic" + str(i) + ".jpg")
#     get_plt("pic" + str(i) + ".jpg")


def main(argv):
    get_centered_ampl_spectrum(argv)
    get_plt(argv)


if __name__ == "__main__":
    args = sys.argv[1:]
    for arg in args:
        main(arg)
