# Domácí úkoly DZO
## Úkol 1 - Monadické operace
[hw1](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/blob/master/hw1)

## Úkol 2 - Fourierova transformace
[hw2](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/blob/master/hw2)

## Úkol 3 - Konvoluce
[hw3](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/blob/master/hw3)

## Úkol 4 - Nelineární filtrace
[hw4](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/blob/master/hw4)

## Úkol 5 - Editace obrazu
[hw5](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/blob/master/hw5)