# Úkol 5 - Editace obrazu

## Spuštění
```bash
python main.py <název podkladového souboru> <název krycího souboru> <maska> <počet iterací> ...
```

## Předpoklady
Pro spuštění programu je třeba mít nainstalované pythnovské knihovny numpy, opencv a tqdm.

```bash
pip install numpy opencv-python tqdm
```

## Běh programu

Program počítá s překrývání celým krycím obrázkem, který má i masku. Z podkladového obrázku se vyřízne výřez o velikosti obrázku krycího a nad ním se provádí úpravy.
Pro ověření správného běhu je výřez uložen jako soubor "cut.jpg".

![cut](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw5/cut.jpg)*Výřez*

Uložen je také soubor prolnutí po aplikaci laplaceova operátoru.

![cut](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw5/b.jpg)*Prolnutí*

Následně proběhne zadaný počet iterací Gauss-Seidelovou metodou, kde se s vyššími hodnotami zlepšuje gradient. Výřez je poté navrácen na původní obrázek.

## Příklady
![original](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw5/renaissance.jpg)*Originální obrázek*
![over](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw5/me.jpg)*Překryv*
![mask](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw5/mask.jpg)*Maska*
![2   ](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw5/merge_me_2_iter.jpg)*2 iterace*   ![10  ](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw5/merge_me_10_iter.jpg)*10 iterací* 
![20  ](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw5/merge_me_20_iter.jpg)*20 iterací*    ![50  ](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw5/merge_me_50_iter.jpg)*50 iterace* 
![200 ](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw5/merge_me_200_iter.jpg)*200 iterací*    ![500 ](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw5/merge_me_500_iter.jpg)*500 iterací*
![900 ](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw5/merge_me_900_iter.jpg)*900 iterací*   ![2000](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw5/merge_me_2000_iter.jpg)*2000 iterací* 
