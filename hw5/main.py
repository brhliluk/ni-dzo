import sys
import cv2
import numpy as np
from tqdm import tqdm

MARGIN = 2
# Offset for positioning image that goes over
offset_x_base = 76
offset_y_base = 70
# For Laplace operator
pixels = [[1, 0], [0, 1], [-1, 0], [0, -1], ]


def edit(image_base, image_over, mask, it):
    width = len(image_over[0])
    height = len(image_over)

    base_cut = get_cut(image_base, width, height)

    # Blend in gradient domain
    b = np.zeros((height, width, 3), np.float)
    for x in range(width):
        for y in range(height):
            # First part of Laplace operator (-4 * arr[y, x])
            # Take original where mask is black
            if mask[y, x] == 0:
                b[y, x] = -4.0 * base_cut[y + MARGIN, x + MARGIN]
            # Take image to be blended where mask is not black
            else:
                b[y, x] = -4.0 * image_over[y, x]
            # Rest of the Laplace operator (arr[y+1, x] + arr[y-1, x] + arr[y, x+1] + arr[y, x-1])
            for i in range(len(pixels)):
                if 0 <= y + pixels[i][1] < height and 0 <= x + pixels[i][0] < width:
                    if mask[y, x] == 0:
                        b[y, x] += base_cut[y + MARGIN + pixels[i][1], x + MARGIN + pixels[i][0]]
                    else:
                        b[y, x] += image_over[y + pixels[i][1], x + pixels[i][0]]
                else:
                    b[y, x] += base_cut[y + MARGIN + pixels[i][1], x + MARGIN + pixels[i][0]]

    cv2.imwrite('cut.jpg', base_cut)
    cv2.imwrite('b.jpg', b)

    # Use overlay image as input for Gauss-Seidel iteration
    new_cut = image_over.copy()
    # Gauss-Seidel iteration
    for _ in tqdm(range(it)):
        for x in range(width):
            for y in range(height):
                # Not black, part to let in
                if mask[y, x] != 0:
                    new_cut[y, x] = (base_cut[y + MARGIN, x + MARGIN + 1] +
                                     base_cut[y + MARGIN, x + MARGIN - 1] +
                                     base_cut[y + MARGIN + 1, x + MARGIN] +
                                     base_cut[y + MARGIN - 1, x + MARGIN] -
                                     b[y, x]) / 4.0
                else:
                    new_cut[y, x] = base_cut[y + MARGIN, x + MARGIN]
        # Place new iteration inside base_cut, which has MARGIN
        base_cut[MARGIN:height+MARGIN, MARGIN:width+MARGIN] = new_cut.copy()

    base_cut = base_cut % 255

    base_cut = base_cut.astype('uint8')

    # Finally desired part with the blend
    for x in range(width):
        for y in range(height):
            image_base[y + offset_y_base, x + offset_x_base] = base_cut[y + MARGIN, x + MARGIN]

    cv2.imwrite('merge_me_' + str(it) + '_iter.jpg', image_base)


def get_cut(img_base, width, height):
    # Cut for blending from base image
    base_cut = img_base[offset_y_base - MARGIN:offset_y_base + height + MARGIN,
                        offset_x_base - MARGIN:offset_x_base + width + MARGIN].copy()
    return base_cut.astype('float')


def main(img_base_name, img_over_name, img_mask_name, iterations):
    image_base = cv2.imread(img_base_name, 1)
    image_over = cv2.imread(img_over_name, 1).astype('float')
    mask = cv2.imread(img_mask_name, 0)
    edit(image_base, image_over, mask, iterations)


# Args:
#   img_base_name = path to the base image
#   img_over_name = path to the image that goes over
#   img_mask_name = path to the mask image
#   iterations = number of iterations for Gauss-Seidel blending
# DO NOT FORGET TO CHANGE OFFSET WHEN CHANGING IMAGES
# IMPLEMENTATION ASSUMES THE IMAGE THAT GOES OVER IS USED WITHOUT CUTTING IT

if __name__ == "__main__":
    args = sys.argv[1:]
    for j in range(0, len(args), 4):
        main(args[j], args[j + 1], args[j + 2], int(args[j + 3]))
