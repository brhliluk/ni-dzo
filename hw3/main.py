import sys
from os.path import splitext

import cv2
from enum import Enum
import numpy as np


class Axis(Enum):
    X = 'x'
    Y = 'y'


def get_gauss(sigma):
    size = sigma * 6 - 1
    return_ker = [np.exp(-float(i - size // 2) ** 2 / (2 * sigma ** 2)) / (sigma * np.sqrt(2 * np.pi)) for i in range(size)]
    return np.asarray(return_ker/np.sum(return_ker))


def apply_ker(axis, ker, image):
    rows, cols = image.shape
    result = np.zeros([rows, cols])
    ker_size = ker.shape[0]
    padding = (ker_size - 1) // 2

    for x in range(rows):
        for y in range(cols):
            pixel = 0
            for k in range(ker_size):
                # reflection padding dependant on given axis
                axis_idx = np.abs((x if axis == Axis.X else y) + k - padding)
                # out of bound indices adjustment
                if axis_idx >= (rows if axis == Axis.X else cols):
                    axis_idx = (rows if axis == Axis.X else cols) - (axis_idx - rows + 2)

                pixel += (image[axis_idx, y] if axis == Axis.X else image[x, axis_idx]) * ker[k]
            result[x, y] = pixel
    return result


def convolve(image, kernel):
    # apply kernel in both axis
    immediate = apply_ker(Axis.X, kernel, image)
    final = apply_ker(Axis.Y, kernel, immediate)
    return final


def main(file, sigma):
    # load image in grayscale, setup gauss kernel
    image = cv2.imread(file, 0)
    kernel = get_gauss(sigma)

    # apply convolution using separate kernel
    image = convolve(image, kernel)

    # save
    name = splitext(file)[0] + "_convoluted_" + str(sigma) + splitext(file)[1]
    print(name)
    cv2.imwrite(name, image)


if __name__ == "__main__":
    args = sys.argv[1:]
    for i in range(0, len(args), 2):
        main(args[i], int(args[i + 1]))
