# Úkol 3 - Konvoluce

## Spuštění
```bash
python main.py <název souboru> <hodnota sigma> <název souboru> <hodnota sigma> ...
```

## Předpoklady
Pro spuštění programu je třeba mít nainstalované pythnovské knihovny enum, numpy, opencv.

```bash
pip install enum numpy opencv-python
```

## Běh programu

Nejdříve se vytvoří `Gaussovská matice` dle zadaného parametru `sigma`. Tu vytvoříme pomocí vzorce: 
```math
f(x) = \frac{1}{\sqrt{2\pi\sigma^{2}}}e^{\frac{-(x-\mu)^{2}}{2\sigma^{2}}}
```
, kde $`\mu`$ je průměr (délka intervalu / 2). Stačí nám ve své jednorozměrné podobě, jelikož ji následně aplikujeme nejdřív zvlášť podle osy X a poté zvlášť podle osy Y. 

## Příklady

![original](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw3/lenna.jpg)*Originální obrázek*
![sigma=1](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw3/examples/lenna_convoluted_1.jpg)*$`\sigma`$=1*
![sigma=5](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw3/examples/lenna_convoluted_5.jpg)*$`\sigma`$=5*
![sigma=10](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw3/examples/lenna_convoluted_10.jpg)*$`\sigma`$=10*

![original](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw3/letter.png)*Originální obrázek*
![sigma=1](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw3/examples/letter_convoluted_1.png)*$`\sigma`$=1*
![sigma=5](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw3/examples/letter_convoluted_5.png)*$`\sigma`$=5*
![sigma=10](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw3/examples/letter_convoluted_10.png)*$`\sigma`$=10*