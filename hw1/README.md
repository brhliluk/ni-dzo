# Úkol 1 - Monadické operace

## Spuštění
```bash
python main.cpp
```

## Předpoklady
Pro spuštění programu je třeba mít nainstalované pythnovské knihovny Pillow, numpy a tkinter.

```bash
pip install Pillow numpy tk
```

Implementace vyžaduje, aby byl ve stejné složce jako je spouštěný soubor, soubor `pic.jpg`, který používá jako defaultní soubor k otevření. Dále si můžete pro práci vybrat libovolný jiný obrázek s příponou `.jpg`.

## Běh programu

Program umí převést zadaný obrázek do jeho znegované podoby. Toto provede po stisknutí tlačítka z jeho GUI. Změny se provádí přímo. Uživateli je také umožněn návrat do původní podoby obrázku.

Edit 7.3. 15:33: Přidány operace zvýšení jasu, snížení jasu, zvýšení a snížení kontrastu a threshold.
Threshold upraví hodnotu každé barvy na 255, pokud je vyšší než 127, jinak ji nastaví na 0.
Jas se zvyšuje a snižuje o hodnotu 50.
Kontrast se navyšuje a snižuje dvojnásobně.

## Příklad

![](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw1/Results/original.png)
![negace](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw1/Results/result.jpg)

![](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw1/testpics/prtscr.png)

![brightness](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw1/Results/brightness.png)
![barevný threshold](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw1/Results/colorthreshold.png)
![kontrast](https://gitlab.fit.cvut.cz/brhliluk/ni-dzo/raw/master/hw1/Results/contrast.png)