# -*- coding: utf-8 -*-
from tkinter.filedialog import askopenfilename
from tkinter import *
import numpy as np
from matplotlib.colors import rgb_to_hsv, hsv_to_rgb
from PIL import Image, ImageTk

IMAGENAME = "pic.jpg"
BACKUPIMG = np.array(Image.open(IMAGENAME))


# --------------SAVE IMAGE, UPDATE DISPLAY--------------
def saveimg(npimage):
    Image.fromarray(npimage).save(IMAGENAME)
    appimg = ImageTk.PhotoImage(Image.fromarray(npimage))
    imageLabel.configure(image=appimg)
    imageLabel.image = appimg


# --------------TURN TO NEGATIVE--------------
def negative():
    npimage = np.array(Image.open(IMAGENAME))
    height = len(npimage)
    width = len(npimage[0])
    # 1 - L for all pixels
    for row in range(height):
        for col in range(width):
            red = 255 - npimage[row][col][0]
            green = 255 - npimage[row][col][1]
            blue = 255 - npimage[row][col][2]
            npimage[row][col] = [red, green, blue]
    saveimg(npimage)


# --------------APPLY THRESHOLD FOR ALL COLORS--------------
def threshold():
    npimage = np.array(Image.open(IMAGENAME))
    height = len(npimage)
    width = len(npimage[0])
    # 1 - L for all pixels
    for row in range(height):
        for col in range(width):
            red = 255 if npimage[row][col][0] > 127 else 0
            green = 255 if npimage[row][col][1] > 127 else 0
            blue = 255 if npimage[row][col][2] > 127 else 0
            npimage[row][col] = [red, green, blue]
    saveimg(npimage)


# --------------TURN UP BRIGHTNESS--------------
def brightnessup():
    npimage = np.array(Image.open(IMAGENAME))
    hsvimage = rgb_to_hsv(npimage)
    hsvimage[:, :, 2] += 50
    hsvimage = np.clip(hsvimage, 0, 255)
    npimage = hsv_to_rgb(hsvimage).astype(np.uint8)
    saveimg(npimage)


# --------------TURN DOWN BRIGHTNESS--------------
def brightnessdown():
    npimage = np.array(Image.open(IMAGENAME))
    hsvimage = rgb_to_hsv(npimage)
    hsvimage[:, :, 2] -= 50
    hsvimage = np.clip(hsvimage, 0, 255)
    npimage = hsv_to_rgb(hsvimage).astype(np.uint8)
    saveimg(npimage)


# --------------TURN UP CONTRAST-------------
def contrastup():
    npimage = np.array(Image.open(IMAGENAME))

    npimage = np.clip(npimage * 2, 0, 255)

    saveimg(npimage)


# --------------TURN DOWN CONTRAST-------------
def contrastdown():
    npimage = np.array(Image.open(IMAGENAME))

    npimage = np.clip(npimage * 0.5, 0, 255).astype(np.uint8)

    saveimg(npimage)


# --------------RESET IMAGE TO ORIGINAL STATE--------------
def reset():
    Image.fromarray(BACKUPIMG).save(IMAGENAME)
    appimage = ImageTk.PhotoImage(Image.fromarray(BACKUPIMG))
    imageLabel.configure(image=appimage)
    imageLabel.image = appimage


# --------------LOAD GIVEN IMAGE--------------
def load_file():
    global IMAGENAME
    global BACKUPIMG
    IMAGENAME = askopenfilename(filetypes=(("Image Files", "*.jpg"),))
    BACKUPIMG = np.array(Image.open(IMAGENAME))
    saveimg(np.array(Image.open(IMAGENAME)))


# --------------CREATE GUI FOR APPLICATION--------------
root = Tk()
filedialogimg = Button(root, text="Open file...", command=load_file)
negativebutton = Button(root, text="Turn to negative", command=negative)
thresholdbutton = Button(root, text="Threshold", command=threshold)
brightnessupbutton = Button(root, text="Turn up brightness", command=brightnessup)
brightnessdownbutton = Button(root, text="Turn down brightness", command=brightnessdown)
contrastupbutton = Button(root, text="Turn up contrast", command=contrastup)
contrastdownbutton = Button(root, text="Turn down contrast", command=contrastdown)
resetbutton = Button(root, text="Reset image", command=reset)

# --------------PLACE GUI--------------
filedialogimg.grid(row=0)
negativebutton.grid(row=1)
thresholdbutton.grid(row=2)
brightnessupbutton.grid(row=3)
brightnessdownbutton.grid(row=4)
contrastupbutton.grid(row=5)
contrastdownbutton.grid(row=6)
resetbutton.grid(row=7)

# --------------OPEN IMAGE--------------
appimage = ImageTk.PhotoImage(Image.open(IMAGENAME))
imageLabel = Label(image=appimage)
imageLabel.grid(row=8)

# --------------PROGRAMS MAIN LOOP--------------
root.mainloop()
